#define MAXNAME 64
#define MAXARR 1024

//Define struct for employee information
struct employeeInfo {
    char first_name[MAXNAME], last_name[MAXNAME];
    int six_digit_ID, salary;
};

//Load file
FILE *fp;
char loadFile(char *fileName){
    if ((fp=fopen(fileName, "r")) == NULL) {
        printf("Error, this file was not found.\n");
        return -1;
    }
    return 0;
}

//sort
int cmp(struct employeeInfo *em1, struct employeeInfo *em2){
    int id1 = em1->six_digit_ID;
    int id2 = em2->six_digit_ID;
    return id1 - id2;
}

//showEmployee
void showEmployee(struct employeeInfo *em, int employeeIndex){
    printf("%-21s %-10s %-10s\n---------------------------------------\n","NAME","SALARY","ID");
    for(int a = 0;a<employeeIndex;a++){
        printf("%-10s %-10s %-10d %-10d\n",
               em[a].first_name,
               em[a].last_name,
               em[a].salary,
               em[a].six_digit_ID
               );
    }
    printf("---------------------------------------\n");
}
void showEmployeeByParam(struct employeeInfo employeeStr[MAXARR],int a){
    printf("%-21s %-10s %-10s\n---------------------------------------\n","NAME","SALARY","ID");
    printf("%-10s %-10s %-10d %-10d\n",
           employeeStr[a].first_name,
           employeeStr[a].last_name,
           employeeStr[a].salary,
           employeeStr[a].six_digit_ID
    );
    printf("---------------------------------------\n");
}

int showEmployeeByName(struct employeeInfo em[MAXARR],int employeeIndex, char name[MAXARR]){
    int i=0;
    for(int i=0;i<employeeIndex+1;i++){
        if(strcmp(em[i].last_name,name)==0){
            return i;
        }
    }
    return -1;
}

int binsearch(int arr[], int len, int src)
{
    int idx = 0,l = 0, r = len-1;
    idx = (l + r)/2;
    while(src != arr[idx])
    {
        if(src < arr[idx])
        {
            r = idx - 1;
        }
        else
        {
            l = idx + 1;
        }
        if(l > r)
        {
            idx = -1;
            break;
        }
        idx = (l + r)/2;
    }
    return idx;
}