#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "readfile.h"
int main(){
    char first_name[MAXNAME], last_name[MAXNAME], file_name[MAXNAME];
    int option, six_digit_ID, salary;
    int employee[MAXARR];
    int employeeIndex = 0;
    int in = 0;
    FILE *fp;
    struct employeeInfo employeeStr[MAXARR];
    printf("please input file name\n");
    gets(file_name);
    char res = loadFile(file_name);
    if(res != -1){
        fp = fopen(file_name,"r");
        while(!feof(fp)){
            fscanf(fp, "%d %s %s %d\n",
                   &employeeStr[employeeIndex].six_digit_ID ,
                   &employeeStr[employeeIndex].first_name,
                   &employeeStr[employeeIndex].last_name,
                   &employeeStr[employeeIndex].salary);
            employeeIndex++;
        }
    }else{
        return 0;
    }
    while (in==0) {
        printf(
                "----------------------------------\n"
                "  (1) Print the Database\n"
                "  (2) Lookup by ID\n"
                "  (3) Lookup by Last Name\n"
                "  (4) Add an Employee\n"
                "  (5) Quit\n"
                "----------------------------------\n"
                "please Enter your choice: \n"
        );
        scanf("%d", &option);
        for (int i = 0; i < employeeIndex + 1; i++) {
            employee[i] = employeeStr[i].six_digit_ID;
        }
        if (option == 1) {
            qsort(employeeStr,employeeIndex,sizeof(employeeStr[0]),cmp);
            showEmployee(employeeStr, employeeIndex);
            printf("Number of Employees (%d)\n", employeeIndex);
        } else if (option == 2) {
            printf("Enter a 6 digit employee id:\n");
            int id;
            scanf("%d", &id);
            int a = binsearch(employee, employeeIndex, id);
            if(a != -1){
                showEmployeeByParam(employeeStr,a);
            }else{
                printf("The employee was not found\n");
            }
        } else if (option == 3) {
            printf("Enter Employee's last name (no extra spaces):\n");
            char name[MAXNAME];
            scanf("%s", &name);
            int a = showEmployeeByName(employeeStr,employeeIndex,name);
            if(a != -1){
                showEmployeeByParam(employeeStr,a);
            }else{
                printf("The employee was not found\n");
            }
        } else if (option == 4) {
            printf("Enter the first name of the employee:\n");
            char firstName[MAXNAME];
            scanf("%s", &firstName);
            printf("Enter the last name of the employee:\n");
            char lastName[MAXNAME];
            scanf("%s", &lastName);
            printf("Enter employee's salary (30000 to 150000):\n");
            int salary;
            scanf("%d", &salary);
            int a = showEmployeeByName(employeeStr,employeeIndex,firstName);
            if(a != -1){
                printf("The name already exists, please re-enter");
            }
            a = showEmployeeByName(employeeStr,employeeIndex,lastName);
            if(a != -1){
                printf("The name already exists, please re-enter");
            }
            if(salary>150000 || salary<30000){
                printf("The salary not in 30000-150000, please re-enter");
            }
            printf("do you want to add the following employee to the DB? %s %s, salary: %d\nEnter 1 for yes, 0 for no:",firstName,lastName,salary);
            int index;
            scanf("%d", &index);
            if(index == 1){
                int id = employeeStr[employeeIndex-1].six_digit_ID+1;
                strcpy(employeeStr[employeeIndex].first_name,firstName);
                strcpy(employeeStr[employeeIndex].last_name,lastName);
                employeeStr[employeeIndex].six_digit_ID = id;
                employeeStr[employeeIndex].salary = salary;
                //employeeStr
                employeeIndex = employeeIndex+1;
            }

        } else if (option == 5) {
            printf("goodbye! \n");
            in = -1;
        } else {
            printf("Hey, %d is not between 1 and 5, try again...\n", option);
        }
    }
    return 0;
}
